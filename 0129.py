# -*- coding: utf-8 -*-

import requests
from stylecloud import gen_stylecloud
import jieba

def getdata():
    headers = {
         'User-Agent':'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:64.0) Gecko/20100101 Firefox/64.0'
    }

    oidlist=['287593212','286279864','285602874','288005981','287970790','287621268']
    for j in oidlist:
        url="https://api.bilibili.com/x/v1/dm/list.so?oid="+str(j)
        r = requests.get(url, headers=headers)
        r.encoding = 'utf-8'
        list_s = r.text.split("<d p=")
        list_s = list_s[1:]
        with open("commit.txt","a+",encoding='utf-8') as f:
            for i in list_s:
                  i = (i.split(">"))[1].replace("</d","")
                  i = i.replace("？","").replace("。","").replace("，","").replace("+","").replace("！","").replace("....","").replace(".......","")
                  f.write(str(i)+"\n")


###1.词云分析
def analysis1():

     with open("commit.txt", 'r', encoding='utf8') as f:
        st = f.read()

        #print(st)
        word_list = jieba.cut(st)
        result = " ".join(word_list)  # 分词用 隔开
        # 制作中文云词
        icon_name = 'fab fa-qq'
        # 图片保存名称
        picp = '1.png'
        gen_stylecloud(text=result,
                       font_path='simsun.ttc',
                       # icon_name='fas fa-envira',
                       icon_name='fas fa-cannabis',
                       max_words=100,
                       max_font_size=70,
                       output_name='icon1.png',
                       )  # 必须加中文字体，否则格式错误


###1.情感分析

###导入相应的库
from snownlp import SnowNLP

def analysis2():

     with open("commit.txt", 'r', encoding='utf8') as f:
         data = f.readlines()

     sum=0
     for i in data:
         sentiments = SnowNLP(i).sentiments
         #print(i)
         sum = sum+sentiments
         #print(sentiments)
         #print("--------------------")
     print("情感分析平均分："+str(sum/len(data)))

     #print(len(data))

#getdata()
#analysis1()
analysis2()